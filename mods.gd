extends Node


const MODS_PATH = "mods/"

var mods_info = {}


func load_mod(filename: String) -> bool:
	var file = File.new()
	if file.file_exists(MODS_PATH + "/%s" % filename):
		ProjectSettings.load_resource_pack(MODS_PATH + "/%s" % filename)
		var mod_server_scene = "res://" + filename.replace(".pck", "_server.tscn")
		var mod_has_server = file.file_exists(mod_server_scene)
		if file.file_exists("res://" + filename.replace(".pck", ".tscn")) or mod_has_server:
			if file.file_exists("res://" + filename.replace(".pck", ".json")):
				var mod_instance
				if mod_has_server:
					mod_instance = load(mod_server_scene).instance()
				else:
					mod_instance = load("res://" + filename.replace(".pck", ".tscn")).instance()
				file.open("res://" + filename.replace(".pck", ".json"), File.READ)
				var mod_manifest = parse_json(file.get_as_text())
				mods_info[filename] = mod_manifest
				call_deferred("add_mod", mod_instance)
				if mod_manifest.keys().has("name") and mod_manifest.keys().has("version"):
					print(mod_manifest["name"] + " successfully loaded, version " + mod_manifest["version"])
					return true
				else:
					print("could find mod scene but manifest was bad")
			else:
				print("could find mod scene but manifest was missing")
			file.close()
			return false
		else:
			file.close()
			return false
	else:
		return false


func add_mod(mod_instance: Node):
	get_tree().get_root().add_child(mod_instance)
